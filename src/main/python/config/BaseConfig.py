
import os
from datetime import timedelta
import tempfile


BASE_DIR = os.path.dirname(os.path.realpath(__file__))


class BaseConfig:
    # Flask 
    ENV = 'development'
    FLASK_ENV = 'development'
    SECRET_KEY = 'c84ceb96dcbb3e24d0db1bc501727a2c8cf6ef631ee18c14709f313fb2e8e8db7578c42f23b40c6b9fe12e7553d5072eee08'
    JWT_SECRET_KEY = 'ZjFmMDM5MWU0MDk0MjQ5YmY0ZjAyNzY5YmZkNGVjNGRlOWUxNzEyZjIyMmNjOWFkOGZhMjIyMjc1OTAyMTNhZjZjMGRhZjFmNzY1OWVhY2IzMjU5OGRlODI3YjY4NGU2NTM1NjNjNzRmZjgyM2Y1MTExNDM2YWM0ZDNkMjhhZWI='
    JWT_ALGORITHM = 'HS512'

    # Database
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = False
    SQLALCHEMY_EXPIRE_ON_COMMIT = False

    # Mail Configurations
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'my-email-id@gmail.com'
    MAIL_PASSWORD = 'my-email-password'

    # Cache Configurations
    
class ProdConfig:
    # Flask 
    ENV = 'production'
    FLASK_ENV = 'production'
    SECRET_KEY = 'c84ceb96dcbb3e24d0db1bc501727a2c8cf6ef631ee18c14709f313fb2e8e8db7578c42f23b40c6b9fe12e7553d5072eee08'
    JWT_SECRET_KEY = 'ZjFmMDM5MWU0MDk0MjQ5YmY0ZjAyNzY5YmZkNGVjNGRlOWUxNzEyZjIyMmNjOWFkOGZhMjIyMjc1OTAyMTNhZjZjMGRhZjFmNzY1OWVhY2IzMjU5OGRlODI3YjY4NGU2NTM1NjNjNzRmZjgyM2Y1MTExNDM2YWM0ZDNkMjhhZWI='
    JWT_ALGORITHM = 'HS512'

    # Database
    # SQLALCHEMY_DATABASE_URI = 'mariadb+mariadbconnector://<user>:<password>@<host>[:<port>]/<dbname>'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = False
    SQLALCHEMY_EXPIRE_ON_COMMIT = False

    # Mail Configurations
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'my-email-id@gmail.com'
    MAIL_PASSWORD = 'my-email-password'

    # Cache Configurations
